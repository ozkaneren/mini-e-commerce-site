﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Panel.Models
{
    public class PagingModel
    {
        public List<Product> Product { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }

    }
}