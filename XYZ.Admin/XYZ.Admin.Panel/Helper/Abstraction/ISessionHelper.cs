﻿namespace XYZ.Admin.Panel.Helper.Abstraction
{
    public interface ISessionHelper
    {
        void Add<T>(T item);
        T Get<T>();
        void Remove<T>();
    }
}
