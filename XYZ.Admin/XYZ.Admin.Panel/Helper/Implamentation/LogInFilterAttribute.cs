﻿using System.Web.Mvc;
using XYZ.Admin.Entity;
using XYZ.Admin.Panel.Helper.Abstraction;

namespace XYZ.Admin.Panel.Helper.Implamentation
{
    public class LogInFilterAttribute:ActionFilterAttribute
    {
        private readonly ISessionHelper _sessionHelper;

        public LogInFilterAttribute()
        {
            _sessionHelper = new SessionHelper();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Staff staff = _sessionHelper.Get<Staff>();
            if (staff == null)
            {
                filterContext.Result = new RedirectResult("/Account/LogIn", false);
            }
        }
    }
}