﻿using System;
using System.Web;
using XYZ.Admin.Panel.Helper.Abstraction;

namespace XYZ.Admin.Panel.Helper.Implamentation
{
    public class SessionHelper : ISessionHelper
    {
        public void Add<T>(T item)
        {
            string sessionKey = typeof(T).Name;

            HttpContext.Current.Session[sessionKey] = item;
        }

        public T Get<T>()
        {
            string sessionKey = typeof(T).Name;

            return (T) HttpContext.Current.Session[sessionKey];
        }

        public void Remove<T>()
        {
            string sessionKey = typeof(T).Name;

            HttpContext.Current.Session[sessionKey] = null;
        }
    }
}