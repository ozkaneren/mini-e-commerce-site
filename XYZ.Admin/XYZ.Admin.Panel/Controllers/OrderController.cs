﻿using System.Collections.Generic;
using System.Web.Mvc;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Business.Implementation;
using XYZ.Admin.Entity;
using XYZ.Admin.Panel.Helper.Abstraction;
using XYZ.Admin.Panel.Helper.Implamentation;

namespace XYZ.Admin.Panel.Controllers
{
    [LogInFilter]
    public class OrderController : Controller
    {
        private readonly IOrderManager _orderManager;
        private readonly ISessionHelper _sessionHelper;
        public OrderController()
        {
            _orderManager = new OrderManager();
            _sessionHelper = new SessionHelper();
        }

        public ActionResult Update(int orderId)
        {

            Order order = _orderManager.GetOrder(orderId);

            return View(order);
        }

        [HttpPost]
        public ActionResult Update(int id, Order order)
        {
            try
            {
                _orderManager.UptadeOrder(order);

                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }

        }

        public ActionResult List()
        {
            List<Order> orders = _orderManager.ListOrders();

            return View(orders);
        }


    }
}
