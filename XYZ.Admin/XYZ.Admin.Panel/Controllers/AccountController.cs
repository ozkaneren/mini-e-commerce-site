﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Business.Implementation;
using XYZ.Admin.Entity;
using XYZ.Admin.Panel.Helper.Abstraction;
using XYZ.Admin.Panel.Helper.Implamentation;

namespace XYZ.Admin.Panel.Controllers
{
   
    public class AccountController : Controller
    {
        private readonly IStaffAuthenticator _staffAuthenticator;

        private readonly ISessionHelper _sessionHelper;

        public AccountController()
        {
            _staffAuthenticator = new StaffAuthenticator();
            _sessionHelper = new SessionHelper();
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(StaffLogIn staffLogIn)
        {
            try
            {
                Staff staff = _staffAuthenticator.Authenticate(staffLogIn);

                _sessionHelper.Add<Staff>(staff);


                return RedirectToAction("Index", "Supplier", new { });


            }
            catch (Exception e)
            {
                return View();
            }
        }
    }
}