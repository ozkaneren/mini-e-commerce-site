﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using XYZ.Admin.Business.Implementation;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Entity;
using XYZ.Admin.Panel.Helper.Implamentation;
using XYZ.Admin.Panel.Models;

namespace XYZ.Admin.Panel.Controllers
{
    [LogInFilter]
    public class ProductController : Controller
    {
        private readonly IProductManager _productManager;
        public const int PageSize = 2;

        public ProductController()
        {
            _productManager = new ProductManager();
        }

        public ActionResult Index(int currentPageIndex = 1)
        {
            return View(this.GetProducts(currentPageIndex));
        }

        public PagingModel GetProducts(int currentPage = 1)
        {
            int maxRows = 15;

           
            List<Product> products = _productManager.ListProducts();
            PagingModel model = new PagingModel();

            model.Product = (from product in products
                    select product)
                .OrderBy(r => r.ProductId)
                .Skip((currentPage - 1) * maxRows)
                .Take(maxRows).ToList();

            double pageCount = (double)((decimal)products.Count() / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);

            model.CurrentPageIndex = currentPage;

            return model;
        }

        public PartialViewResult Search(string q)
        {
            List<Product> products = new List<Product>();
            products = _productManager.ListProducts();

            if (q.IsNullOrWhiteSpace())
            {
                var search = products.Where(r => r.Name.Contains(q) ||
                                                 String.IsNullOrEmpty(q)).Take(1000);
                return PartialView("ProductSearchResults", search);
            }
            else
            {
                if (q.Substring(0, 1).Equals(q.Substring(0, 1).ToUpper()))
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToLower();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(1000);

                    return PartialView("ProductSearchResults", search);
                }
                else
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToUpper();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(1000);

                    return PartialView("ProductSearchResults", search);
                }

            }

        }
        
        public ActionResult ListProduct()
        {
            List<Product> productlList = new List<Product>();

            productlList = _productManager.ListProducts();

            return View(productlList);
        }
        
    }
}
