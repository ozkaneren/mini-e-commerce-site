﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Business.Implementation;
using XYZ.Admin.Entity;
using XYZ.Admin.Panel.Helper.Implamentation;

namespace XYZ.Admin.Panel.Controllers
{
    [LogInFilter]
    public class SupplierController : Controller
    {
        private readonly ISupplierManager _supplierManager;

        public SupplierController()
        {
            _supplierManager = new SupplierManager();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(Entity.Supplier supplier)
        {
            try
            {
                _supplierManager.CreateSupplier(supplier);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult Update(int id)
        {
            Entity.Supplier supplier = new Entity.Supplier();
            supplier = _supplierManager.GetSupplier(id);
            return View(supplier);
        }

        [HttpPost]
        public ActionResult Update(int id, Entity.Supplier supplier)
        {
            try
            {
                // TODO: Add update logic here
                _supplierManager.UpdateSupplier(supplier);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            Entity.Supplier supplier;

            supplier = _supplierManager.GetSupplier(id);

            return View(supplier);
        }

        [HttpPost]
        public ActionResult Delete(int id, Entity.Supplier supplier)
        {
            try
            {
                _supplierManager.DeleteSupplier(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult List()
        {
            List<Entity.Supplier> suppliers = new List<Entity.Supplier>();
 
            suppliers = _supplierManager.ListSuppliers();

            return View(suppliers);
        }

    }
}
