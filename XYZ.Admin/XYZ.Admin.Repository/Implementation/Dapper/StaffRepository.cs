﻿using System.Collections;
using System.Data.SqlClient;
using Dapper;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;
using System.Collections.Generic;
using System.Linq;

namespace XYZ.Admin.Repository.Implementation.Dapper
{
    public class StaffRepository : IStaffRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public Staff IsAuthenticated(string eMail, string password)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Staff> staff = con.Query<Staff>("SELECT * FROM Staff WHERE Email = @Email AND Password = @Password", new
                {
                    Email = eMail,
                    Password = password
                });
                return staff.FirstOrDefault();
            }
        }
    }
}
