﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;

namespace XYZ.Admin.Repository.Implementation.Dapper
{
    public class OrderRepository: IOrderRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";
        public List<Order> ListOrders()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Order> orders = con.Query<Order>(@"SELECT * FROM [Order]");

                return orders.ToList();
            }
        }

        public void UptadeOrder(Order order)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"UPDATE [Order] SET BasketId = @BasketId, Status = @Status, Date = @Date, ShipperId = @ShipperId", new
                {
                    BasketId = order.BasketId,
                    Status = order.Status,
                    Date = order.Date,
                    ShipperId = order.ShipperId
                });
            }
        }

        public Order GetOrder(int orderId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Order> order =con.Query<Order>(@"SELECT * FROM [Order] WHERE OrderId = @OrderId", new
                {
                    OrderId = orderId
                });

                return order.FirstOrDefault();
            }
        }

      
    }
}
