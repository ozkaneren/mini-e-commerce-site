﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;

namespace XYZ.Admin.Repository.Implementation.Dapper
{
    public class ProductRepository: IProductRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public List<Product> ListProducts()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> products = con.Query<Product>(@"SELECT * FROM Product");

                return products.ToList();
            }
        }
    }
}
