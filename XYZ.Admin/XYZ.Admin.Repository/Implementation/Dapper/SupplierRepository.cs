﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;

namespace XYZ.Admin.Repository.Implementation.Dapper
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";


        public void UpdateSupplier(Supplier supplier)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"UPDATE Supplier SET Email = @EMail, Password = @Password, Name = @Name, Phone = @Phone WHERE SupplierId = @SupplierId", new
                {
                    Email = supplier.Email,
                    Password = supplier.Password,
                    Name = supplier.Name,
                    Phone = supplier.Phone,
                    SupplierId = supplier.SupplierId
                });
            }
        }

        public void CreateSupplier(Supplier supplier)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"INSERT INTO Supplier (Email, Password, Name, Phone) VALUES (@Email, @Password,  @Name, @Phone)", new
                {
                    Email = supplier.Email,
                    Password = supplier.Password,
                    Name =  supplier.Name,
                    Phone = supplier.Phone,

                });
            }
        }

        public void DeleteSupplier(int supplierId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"DELETE FROM Supplier WHERE SupplierId = @SupplierId", new
                {
                    SupplierId = supplierId
                });
            }
        }

        public List<Supplier> ListSuppliers()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Supplier> suppliers = con.Query<Supplier>(@"SELECT * FROM Supplier");

                return suppliers.ToList();
            }
        }

        public Supplier GetSupplier(int supplierId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Supplier> supplier =
                    con.Query<Supplier>("SELECT * FROM Supplier WHERE SupplierId = @SupplierId", new
                    {
                        SupplierId = supplierId
                    });
                return supplier.FirstOrDefault();
            }
        }
    }
}
