﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Repository.Abstraction
{
    public interface ISupplierRepository
    {
        void UpdateSupplier(Supplier supplier);
        void CreateSupplier(Supplier supplier);
        void DeleteSupplier(int supplierId);
        List<Supplier> ListSuppliers();
        Supplier GetSupplier(int supplierId);
    }
}
