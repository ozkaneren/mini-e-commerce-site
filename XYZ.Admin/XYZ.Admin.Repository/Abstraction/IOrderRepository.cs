﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Repository.Abstraction
{
    public interface IOrderRepository
    {
        List<Order> ListOrders();
        void UptadeOrder(Order order);
        Order GetOrder(int orderId);

    }
}
