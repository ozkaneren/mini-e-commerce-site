﻿using XYZ.Admin.Entity;

namespace XYZ.Admin.Repository.Abstraction
{
    public interface IStaffRepository
    {
        Staff IsAuthenticated(string eMail, string password);
    }
}
