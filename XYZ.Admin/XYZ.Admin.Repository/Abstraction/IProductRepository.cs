﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Repository.Abstraction
{
    public interface IProductRepository
    {
        List<Product> ListProducts();
    }
}
