﻿namespace XYZ.Admin.Entity
{
    public class Staff
    {
        public int StaffId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
