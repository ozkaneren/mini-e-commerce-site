﻿using XYZ.Admin.Entity;

namespace XYZ.Admin.Business.Abstraction
{
    public interface IStaffAuthenticator
    {
        Staff Authenticate(StaffLogIn staffLogIn);
    }
}
