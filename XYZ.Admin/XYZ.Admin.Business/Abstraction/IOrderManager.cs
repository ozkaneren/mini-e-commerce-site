﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Business.Abstraction
{
    public interface IOrderManager
    {
        List<Order> ListOrders();
        void UptadeOrder(Order order);
        Order GetOrder(int orderıD);
    }
}
