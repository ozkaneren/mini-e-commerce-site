﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Business.Abstraction
{
    public interface ISupplierManager
    {
        void UpdateSupplier(Supplier supplier);
        void CreateSupplier(Supplier supplier);
        void DeleteSupplier(int supplierId);
        List<Supplier> ListSuppliers();
        Supplier GetSupplier(int supplierId);
    }
}
