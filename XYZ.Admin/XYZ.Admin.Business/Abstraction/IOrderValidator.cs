﻿namespace XYZ.Admin.Business.Abstraction
{
    public interface IOrderValidator
    {
        bool ValidateOrder();
    }
}
