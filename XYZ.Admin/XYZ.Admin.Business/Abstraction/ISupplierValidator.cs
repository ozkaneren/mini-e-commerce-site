﻿namespace XYZ.Admin.Business.Abstraction
{
    public interface ISupplierValidator
    {
        bool ValidateSupplier();
    }
}
