﻿using System.Collections.Generic;
using XYZ.Admin.Entity;

namespace XYZ.Admin.Business.Abstraction
{
    public interface IProductManager
    {
        List<Product> ListProducts();
    }
}
