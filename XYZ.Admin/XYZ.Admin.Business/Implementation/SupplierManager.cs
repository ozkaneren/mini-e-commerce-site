﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;
using XYZ.Admin.Repository.Implementation.Dapper;

namespace XYZ.Admin.Business.Implementation
{
    public class SupplierManager : ISupplierManager
    {
        private readonly ISupplierRepository _supplierRepository;
        private readonly ISupplierValidator _supplierValidator;

        public SupplierManager()
        {
            _supplierRepository = new SupplierRepository();
            _supplierValidator = new SupplierValidator();
        }
        public void UpdateSupplier(Supplier supplier)
        {
            bool isValid = _supplierValidator.ValidateSupplier();

            if (isValid)
            {
                _supplierRepository.UpdateSupplier(supplier);
            }

        }

        public void CreateSupplier(Supplier supplier)
        {
            bool isValid = _supplierValidator.ValidateSupplier();

            if (isValid)
            {
                _supplierRepository.CreateSupplier(supplier);
            }
        }

        public void DeleteSupplier(int supplierId)
        {
            _supplierRepository.DeleteSupplier(supplierId);
        }

        public List<Supplier> ListSuppliers()
        {
            List<Supplier> suppliers;

            suppliers = _supplierRepository.ListSuppliers();

            return suppliers;
        }

        public Supplier GetSupplier(int supplierId)
        {
            Supplier supplier = new Supplier();

            bool isValid = _supplierValidator.ValidateSupplier();

            if (isValid)
            {
                supplier = _supplierRepository.GetSupplier(supplierId);
            }

            return supplier;
        }
    }
}
