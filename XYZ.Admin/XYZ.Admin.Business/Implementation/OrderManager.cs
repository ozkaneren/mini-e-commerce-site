﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;
using XYZ.Admin.Repository.Implementation.Dapper;

namespace XYZ.Admin.Business.Implementation
{
    public class OrderManager : IOrderManager
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderValidator _orderValidator;
        public OrderManager()
        {
            _orderRepository = new OrderRepository();
            _orderValidator = new OrderValidator();
        }
        public List<Order> ListOrders()
        {
            List<Order> orders;

            orders = _orderRepository.ListOrders();

            return orders;
        }

        public void UptadeOrder(Order order)
        {
            bool isValid = _orderValidator.ValidateOrder();

            if (isValid)
            {
                _orderRepository.UptadeOrder(order);
            }

        }

        public Order GetOrder(int orderId)
        {
            Order order = new Order();

            bool isValid = _orderValidator.ValidateOrder();

            if (isValid)
            {
                order = _orderRepository.GetOrder(orderId);
            }

            return order;
        }
    }
}
