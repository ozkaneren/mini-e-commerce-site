﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;
using XYZ.Admin.Repository.Implementation.Dapper;

namespace XYZ.Admin.Business.Implementation
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;
        public ProductManager()
        {
            _productRepository = new ProductRepository();
        }
        public List<Product> ListProducts()
        {
            List<Product> products;

            products = _productRepository.ListProducts();

            return products;
        }
    }
}
