﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XYZ.Admin.Business.Abstraction;
using XYZ.Admin.Entity;
using XYZ.Admin.Repository.Abstraction;
using XYZ.Admin.Repository.Implementation.Dapper;

namespace XYZ.Admin.Business.Implementation
{
    public class StaffAuthenticator : IStaffAuthenticator
    {
        private readonly IStaffRepository _staffRepository;
        public StaffAuthenticator()
        {
            _staffRepository = new StaffRepository();

        }
        public Staff Authenticate(StaffLogIn staffLogIn)
        {
            Staff staff = new Staff();

            staff = _staffRepository.IsAuthenticated(staffLogIn.Email,staffLogIn.Password);

            return staff;
        }
    }
}
