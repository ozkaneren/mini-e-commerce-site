﻿using System;

namespace XYZ.Supplier.Entity
{
    public class Order
    {
        public int OrderId { get; set; }
        public int BasketId { get; set; }
        public int Status { get; set; }
        public DateTime Date { get; set; }
        public int ShipperId { get; set; }


    }
}
