﻿namespace XYZ.Supplier.Entity
{
    public class SupplierLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}