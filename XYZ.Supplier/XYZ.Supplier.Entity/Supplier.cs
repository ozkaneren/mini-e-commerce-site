﻿namespace XYZ.Supplier.Entity
{
   public class Supplier
    {
        public int SupplierId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int Phone { get; set; }
    }
}
