﻿using System.Collections.Generic;
using XYZ.Supplier.Business.Abstraction;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Repository.Abstraction;
using XYZ.Supplier.Repository.Implementation.Dapper;

namespace XYZ.Supplier.Business.Implementation
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductLogger _productLogger;
        private readonly IProductValidator _productValidator;


        public ProductManager()
        {
            _productLogger = new ProductFileLogger();
            _productRepository = new ProductRepository();
            _productValidator = new ProductValidator();

        }

        public void AddProduct(Product product)
        {
            bool isValid = _productValidator.ValidateProduct(product);

            if (isValid)
            {
                _productRepository.AddProduct(product);
            }
        }

        public void UpdateProduct(Product product)
        {
            bool isValid = _productValidator.ValidateProduct(product);

            if (isValid && product != null)
            {
                _productRepository.UpdateProduct(product);
            }
        }

        public void DeleteProduct(int productId)
        {

            bool isValid = _productValidator.ValidateProduct(productId);

            if (isValid)
            {
                _productRepository.DeleteProduct(productId);
            }


        }

        public List<Product> GetProducts(int supplierId)
        {
            List<Product> productList = _productRepository.GetProducts(supplierId);

            return productList;
        }

        public Product GetProduct(int productId)
        {
            Product product = _productRepository.GetProduct(productId);

            return product;
        }
    }
}
