﻿using System.Collections.Generic;
using XYZ.Supplier.Business.Abstraction;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Repository.Abstraction;
using XYZ.Supplier.Repository.Implementation.Dapper;

namespace XYZ.Supplier.Business.Implementation
{
    public class OrderManager:IOrderManager
    {

        private readonly IOrderRepository _orderRepository;
        private readonly IOrderValidator _orderValidator;

        public OrderManager()
        {
            _orderValidator = new OrderValidator();
            _orderRepository = new OrderRepository();
            
        }
        
        public List<Order> GertOrders(int supplierId)
        {
            List<Order> orderList =_orderRepository.GetOrders(supplierId);
            return orderList;
        }

        public void DeleteOrder(int orderId)
        {
            bool isValid = _orderValidator.ValidateOrder(orderId);
            if (isValid)
            {
                _orderRepository.DeleteOrder(orderId); 
            }
        }
    }
}
