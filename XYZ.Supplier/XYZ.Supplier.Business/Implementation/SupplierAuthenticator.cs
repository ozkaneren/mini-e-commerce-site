﻿using System;
using XYZ.Supplier.Business.Abstraction;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Repository.Abstraction;
using XYZ.Supplier.Repository.Implementation.Dapper;

namespace XYZ.Supplier.Business.Implementation
{
    public class SupplierAuthenticator : ISupplierAuthenticator
    {
        private readonly ISupplierRepository _supplierRepository;

        public SupplierAuthenticator()
        {
            _supplierRepository = new SupplierRepository();
        }

        public Entity.Supplier Authenticate(SupplierLogin supplierLogin)
        {
            try
            {
                Entity.Supplier supplier = _supplierRepository.IsAuthenticated(supplierLogin.Email, supplierLogin.Password);

                return supplier;
            }
            catch
            {
                return null;
            }
        }
    }
}
