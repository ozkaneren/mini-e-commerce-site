﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Business.Abstraction
{
    public interface IOrderManager
    {
        List<Order> GertOrders(int supplierId);
        void DeleteOrder(int orderId);

    }
}
