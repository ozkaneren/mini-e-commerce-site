﻿using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Business.Abstraction
{
    public interface IProductLogger
    {
        void LogProduct(Product product);
    }
}
