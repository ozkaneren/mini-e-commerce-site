﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Business.Abstraction
{
    public interface ISupplierAuthenticator
    {
        Entity.Supplier Authenticate(SupplierLogin supplierLogin);
    }
}
