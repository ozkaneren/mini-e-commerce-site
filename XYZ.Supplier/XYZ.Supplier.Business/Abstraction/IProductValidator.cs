﻿using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Business.Abstraction
{
    public interface IProductValidator
    {
        bool ValidateProduct(Product product);
        bool ValidateProduct(int productId);
        
    }
}
