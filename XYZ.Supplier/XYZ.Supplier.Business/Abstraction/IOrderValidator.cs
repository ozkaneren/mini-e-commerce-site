﻿namespace XYZ.Supplier.Business.Abstraction
{
    public interface IOrderValidator
    {
        bool ValidateOrder(int orderId);
    }
}
