﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Business.Abstraction
{
    public interface IProductManager
    {
        void AddProduct(Product product);

        void UpdateProduct(Product product);

        void DeleteProduct(int productId);

        List<Product> GetProducts(int supplierId);

        Product GetProduct(int productId);


    }
}
