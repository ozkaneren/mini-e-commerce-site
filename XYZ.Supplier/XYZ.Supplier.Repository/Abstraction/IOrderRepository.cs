﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Repository.Abstraction
{
    public interface IOrderRepository
    {

        List<Order> GetOrders(int supplierId);
        void DeleteOrder(int orderId);
    }
}
