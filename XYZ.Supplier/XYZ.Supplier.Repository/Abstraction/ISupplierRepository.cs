﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Repository.Abstraction
{
    public interface ISupplierRepository
    {
        Entity.Supplier IsAuthenticated(string eMail, string password);
    }
}
 