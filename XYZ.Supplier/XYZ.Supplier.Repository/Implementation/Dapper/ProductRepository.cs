﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Repository.Abstraction;

namespace XYZ.Supplier.Repository.Implementation.Dapper
{
    public class ProductRepository : IProductRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public void AddProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"INSERT INTO Product (Barcode,Name,Size,ColorId,CategoryId,Stock,Price,SupplierId) 
                VALUES (@Barcode, @Name, @Size, @ColorId, @CategoryId, @Stock, @Price, @SupplierId)", new
                {
                    Barcode = product.Barcode,
                    Name = product.Name,
                    Size = product.Size,
                    ColorId = product.ColorId,
                    CategoryId = product.CategoryId,
                    Stock = product.Stock,
                    Price = product.Price,
                    SupplierId = product.SupplierId
                });
            }
        }

        public void UpdateProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(
                    "UPDATE Product SET Barcode = @Barcode, [Name] = @Name, Size = @Size, ColorId = @ColorId, CategoryId = @CategoryId, Stock = @Stock, Price = @Price, SupplierId = @SupplierId WHERE ProductId = @ProductId",
                    new
                    {
                        Barcode = product.Barcode,
                        Name = product.Name,
                        Size = product.Size,
                        ColorId = product.ColorId,
                        CategoryId = product.CategoryId,
                        Stock = product.Stock,
                        ProductId = product.ProductId,
                        Price = product.Price,
                        SupplierId = product.SupplierId
                    });
            }
        }

        public void DeleteProduct(int productId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("DELETE FROM Product WHERE ProductId = @ProductId", new
                {
                    ProductId = productId
                });
            }
        }

        public List<Product> GetProducts(int supplierId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> products = con.Query<Product>("SELECT * FROM Product WHERE SupplierId = @SupplierId", new
                {
                    SupplierId = supplierId
                });
                return products.ToList();
            }
        }

        public Product GetProduct(int productId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> product = con.Query<Product>(@"SELECT * FROM Product WHERE ProductId = @ProductId", new
                {
                    ProductId = productId
                });
                return product.FirstOrDefault();
            }
        }
    }
}
