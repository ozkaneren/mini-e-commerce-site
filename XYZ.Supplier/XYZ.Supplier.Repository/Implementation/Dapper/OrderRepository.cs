﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Repository.Abstraction;

namespace XYZ.Supplier.Repository.Implementation.Dapper
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public List<Order> GetOrders(int supplierId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Order> orders = con.Query<Order>(@"
                    SELECT o.* 
                    FROM Supplier as s 
                    INNER JOIN Product as p ON s.SupplierId = p.SupplierId 
                    INNER JOIN BasketItem as bi ON p.ProductId = bi.ProductId 
                    INNER JOIN Basket as b ON bi.BasketId = b.BasketId 
                    INNER JOIN [Order] as o ON b.BasketId = o.BasketId");
                return orders.ToList();
            }
        }

        public void DeleteOrder(int orderId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("DELETE FROM Order WHERE OrderId = @OrderId", new
                {
                    OrderId = orderId
                });
            }
        }
        
    }
}
