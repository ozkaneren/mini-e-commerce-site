﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Supplier.Repository.Abstraction;

namespace XYZ.Supplier.Repository.Implementation.Dapper
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public Entity.Supplier IsAuthenticated(string eMail, string password)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    IEnumerable<Entity.Supplier> supplier = con.Query<Entity.Supplier>("SELECT * FROM Supplier WHERE Email = @Email AND Password= @Password", new
                    {
                        Email = eMail,
                        Password = password
                    });

                    return supplier.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }

    }
}
