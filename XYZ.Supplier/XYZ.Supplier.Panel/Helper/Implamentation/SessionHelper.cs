﻿using System.Web;
using XYZ.Supplier.Panel.Helper.Abstraction;

namespace XYZ.Supplier.Panel.Helper.Implamentation
{
    public class SessionHelper : ISessionHelper
    {
        public void Add<T>(T item)
        {
            string sessionKey = typeof(T).ToString();

            HttpContext.Current.Session[sessionKey] = item;
        }

        public T Get<T>()
        {
            string sessionKey = typeof(T).ToString();

            return (T) HttpContext.Current.Session[sessionKey];
        }

        public void Remove<T>()
        {
            string sessionKey = typeof(T).ToString();

            HttpContext.Current.Session[sessionKey] = null;
            
        }
    }
}