﻿using System.Web.Mvc;
using XYZ.Supplier.Panel.Helper.Abstraction;

namespace XYZ.Supplier.Panel.Helper.Implamentation
{
    public class LogInFilterAttribute : ActionFilterAttribute
    {
        private readonly ISessionHelper _sessionHelper;
        public LogInFilterAttribute()
        {
            _sessionHelper = new SessionHelper();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Entity.Supplier user = _sessionHelper.Get<Entity.Supplier>();
            if (user == null)
            {
                filterContext.Result = new RedirectResult("/Account/LogIn", false);
            }
        }

    }
}
