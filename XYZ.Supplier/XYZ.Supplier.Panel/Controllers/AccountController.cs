﻿using System.Web.Mvc;
using XYZ.Supplier.Business.Abstraction;
using XYZ.Supplier.Business.Implementation;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Panel.Helper.Abstraction;
using XYZ.Supplier.Panel.Helper.Implamentation;

namespace XYZ.Supplier.Panel.Controllers
{
    public class AccountController : Controller
    {
        private readonly ISupplierAuthenticator _supplierAuthenticator;
        private readonly ISessionHelper _sessionHelper;

        public AccountController()
        {
            _supplierAuthenticator = new SupplierAuthenticator();
            _sessionHelper = new SessionHelper();
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(SupplierLogin supplierLogin)
        {
            try
            {
                Entity.Supplier supplier = _supplierAuthenticator.Authenticate(supplierLogin);

                _sessionHelper.Add<Entity.Supplier>(supplier);
                //...
                //...

                return RedirectToAction("Index", "Product", new {});

            }
            catch
            {
                return View();
            }
        }

        public ActionResult LogOut()
        {
            _sessionHelper.Remove<Entity.Supplier>();

            return RedirectToAction("LogIn");
        }


    }
}
