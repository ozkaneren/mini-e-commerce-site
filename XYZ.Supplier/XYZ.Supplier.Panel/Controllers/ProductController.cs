﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using XYZ.Supplier.Business.Abstraction;
using XYZ.Supplier.Business.Implementation;
using XYZ.Supplier.Entity;
using XYZ.Supplier.Panel.Helper;
using XYZ.Supplier.Panel.Helper.Abstraction;
using XYZ.Supplier.Panel.Helper.Implamentation;
using XYZ.Supplier.Panel.Models;

namespace XYZ.Supplier.Panel.Controllers
{
    [LogInFilter]
    public class ProductController : Controller
    {
        private readonly IProductManager _productManager;
        private readonly ISessionHelper _sessionHelper;
        public const int PageSize = 2;
        public ProductController()
        {
            _productManager = new ProductManager();
            _sessionHelper = new SessionHelper();
        }

        public ActionResult Index(int currentPageIndex = 1)
        {
            return View(this.GetProducts(currentPageIndex));
        }

        public PagingModel GetProducts(int currentPage = 1)
        {
            int maxRows = 3;

            Entity.Supplier supplier = _sessionHelper.Get<Entity.Supplier>();
            List<Product> products = _productManager.GetProducts(supplier.SupplierId);
            PagingModel model = new PagingModel();

            model.Product = (from product in products
                             select product)
                .OrderBy(r => r.ProductId)
                .Skip((currentPage - 1) * maxRows)
                .Take(maxRows).ToList();

            double pageCount = (double)((decimal)products.Count() / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);

            model.CurrentPageIndex = currentPage;

            return model;
        }

        public PartialViewResult Search(string q)
        {
            Entity.Supplier supplier = _sessionHelper.Get<Entity.Supplier>();
            List<Product> products = _productManager.GetProducts(supplier.SupplierId);

            if (q.IsNullOrWhiteSpace())
            {
                var search = products.Where(r => r.Name.Contains(q) ||
                                                 String.IsNullOrEmpty(q)).Take(100);
                return PartialView("ProductSearchResults", search);
            }
            else
            {
                if (q.Substring(0, 1).Equals(q.Substring(0, 1).ToUpper()))
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToLower();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(100);

                    return PartialView("ProductSearchResults", search);
                }
                else
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToUpper();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(100);

                    return PartialView("ProductSearchResults", search);
                }

            }

        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            Product product = _productManager.GetProduct(id);
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {

            try
            {
                Entity.Supplier supplier = _sessionHelper.Get<Entity.Supplier>();
                product.SupplierId = supplier.SupplierId;
                _productManager.AddProduct(product);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View();
                throw;
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            Product product = _productManager.GetProduct(id);
            return View(product);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product product)
        {
            try
            {
                product.ProductId = id;
                product.SupplierId = _sessionHelper.Get<Entity.Supplier>().SupplierId;
                _productManager.UpdateProduct(product);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            Product product = _productManager.GetProduct(id);
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Product product)
        {
            try
            {
                _productManager.DeleteProduct(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult List()
        {
            Entity.Supplier supplier = _sessionHelper.Get<Entity.Supplier>();

            List<Product> productList = _productManager.GetProducts(supplier.SupplierId);

            return View(productList);
        }
    }
}
