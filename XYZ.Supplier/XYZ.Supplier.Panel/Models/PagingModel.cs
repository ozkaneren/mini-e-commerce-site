﻿using System.Collections.Generic;
using XYZ.Supplier.Entity;

namespace XYZ.Supplier.Panel.Models
{
    public class PagingModel
    {
        public List<Product> Product { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }

    }
}