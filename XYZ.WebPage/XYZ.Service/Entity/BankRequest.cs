namespace XYZ.Service.Entity
{
    public class BankRequest
    {
        public decimal Amount { get; set; }

        public int InstallmentCount { get; set; }

        public BankCard BankCard { get; set; }
    }
}