﻿namespace XYZ.Service.Entity
{
    public  class BankResult
    {
        public bool IsSuccessful { get; set; }

        public string ResultCode { get; set; }

        public string ResultMessage { get; set; }
    }
}
