namespace XYZ.Service.Entity
{
    public class BankCard
    {
        public string CardNumber { get; set; }
        public string ExpMonth { get; set; }
        public string ExpYear { get; set; }
        public string CvcNumber { get; set; }
    }
}