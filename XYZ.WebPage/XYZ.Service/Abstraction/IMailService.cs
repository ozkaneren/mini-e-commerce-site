using XYZ.Service.Entity;

namespace XYZ.Service.Abstraction
{
    public interface IMailService
    {
        void SendDoPaymentMail(DoPaymentMailRequest doPaymentMailRequest);
    }
}