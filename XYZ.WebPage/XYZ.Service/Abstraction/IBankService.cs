﻿using XYZ.Service.Entity;

namespace XYZ.Service.Abstraction
{
    public interface IBankService
    {
        BankResult DoPayment(BankRequest bankRequest);
    }
}
