using System;
using XYZ.Service.Abstraction;
using XYZ.Service.Entity;

namespace XYZ.Service.Implementation
{
    public class YapıKrediBank : IBankService
    {
        private readonly Random _bankSimulator;

        public YapıKrediBank()
        {
            _bankSimulator = new Random();
        }
        public BankResult DoPayment(BankRequest bankRequest)
        {
            BankResult result = new BankResult();

            int bankResult = _bankSimulator.Next(1, 10);

            if (bankResult > 3)
            {
                result.IsSuccessful = true;
                result.ResultCode = "00";
                result.ResultMessage = "";
            }
            else
            {
                result.IsSuccessful = false;
                result.ResultCode = bankResult.ToString();
                result.ResultMessage = $"Hata! {bankResult}";
            }

            return result;
        }
    }
}