﻿namespace XYZ.Entities
{
    public class BasketItem
    {
        public int BasketItemId { get; set; }
        public int BasketId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        
    }
}
