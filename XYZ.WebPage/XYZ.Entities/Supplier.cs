﻿namespace XYZ.Entities
{
    public class Supplier
    {
        public int SupplierId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int Phone { get; set; }

    }
}
