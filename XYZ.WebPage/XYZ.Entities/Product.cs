﻿namespace XYZ.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Barcode { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public int ColorId { get; set; }
        public int SupplierId { get; set; }
        public int Category { get; set; }
        public int Stock { get; set; }
        public decimal Price { get; set; }
    }
}
