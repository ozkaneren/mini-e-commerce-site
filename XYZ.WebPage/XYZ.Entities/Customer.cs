﻿namespace XYZ.Entities
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
