﻿using System;
using System.Collections.Generic;

namespace XYZ.Entities
{
    public class Basket
    {
        public int BasketId { get; set; }
        public int CustomerId { get; set; }
        public DateTime Date { get; set; }
        public int Status { get; set; }
        public List<BasketItemProduct> BasketItemProducts { get; set; }
    }
}
