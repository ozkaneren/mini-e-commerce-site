﻿using System;

namespace XYZ.Entities
{
    public class OrderPayment
    {
        public int OrderPaymentId { get; set; }
        public int OrderId { get; set; }
        public decimal Amount { get; set; }
        public string Provider { get; set; }
        public DateTime Date { get; set; }
        public int BinNumber { get; set; }
        public int LastFour { get; set; }
        public int ExpYear { get; set; }
        public int CvcNumber { get; set; }
        public int PaymentStatusId { get; set; }
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public int ExpMonth { get; set; }

    }
}
