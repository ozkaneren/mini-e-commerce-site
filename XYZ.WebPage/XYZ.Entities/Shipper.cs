﻿namespace XYZ.Entities
{
    public class Shipper
    {
        public int ShipperId { get; set; }
        public string ShipperName { get; set; }
    }
}
