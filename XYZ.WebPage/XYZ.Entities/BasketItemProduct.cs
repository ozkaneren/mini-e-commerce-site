﻿namespace XYZ.Entities
{
    public class BasketItemProduct
    {
        public BasketItem BasketItem { get; set; }
        public Product Product { get; set; }
    }
}
