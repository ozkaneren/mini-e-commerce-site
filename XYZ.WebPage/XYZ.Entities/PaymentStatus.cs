﻿namespace XYZ.Entities
{
    public class PaymentStatus
    {
        public int PaymentStatusId { get; set; }
        public string PaymentType { get; set; }
    }
}
