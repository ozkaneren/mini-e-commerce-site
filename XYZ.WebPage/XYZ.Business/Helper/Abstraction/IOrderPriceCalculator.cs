﻿namespace XYZ.Client.Helper.Abstraction
{
    public interface IOrderPriceCalculator
    {
        decimal CalculateOrderPrice(int customerId);
    }
}
