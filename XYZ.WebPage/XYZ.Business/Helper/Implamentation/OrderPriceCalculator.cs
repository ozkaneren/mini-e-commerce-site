﻿using System.Collections.Generic;
using System.Linq.Expressions;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;

namespace XYZ.Business.Helper.Implamentation
{
    public class OrderPriceCalculator : IOrderPriceCalculator
    {
     
        private readonly IBasketManager _basketManager;
        decimal totalAmount;
        public OrderPriceCalculator(IBasketManager basketManager)
        {

            _basketManager = basketManager;
        }
        public decimal CalculateOrderPrice(int customerId)
        {
            totalAmount = 0;
            Basket basket = _basketManager.GetBasket(customerId);
            List<Product> products = _basketManager.GetBasketItemsWithProductList(basket.BasketId);
            List<BasketItem> basketItems = _basketManager.GetBasketItemsWithBasketItemList(basket.BasketId);
            for (int index = 0; index < products.Count; index++)
            {
                totalAmount = totalAmount + products[index].Price * basketItems[index].Qty;
            }
            return totalAmount;
        }
    }
}