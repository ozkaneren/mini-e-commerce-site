﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IShipperManager
    {
        List<Shipper> GetShippers();
    }
}
