﻿using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IOrderManager
    {
        Order GetOrder(int orderId);
        void UpdateOrder(int orderId, int status);
        void CreateOrder(int customerId, Order order);
        Order GetOrderWithBasketId(int basketId);


    }
}
