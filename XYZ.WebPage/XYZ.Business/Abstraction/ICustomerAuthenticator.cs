﻿using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface ICustomerAuthenticator
    {
        Customer IsAuthenticated(string eMail, string password);
    }
}
