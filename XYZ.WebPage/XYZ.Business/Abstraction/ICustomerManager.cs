﻿using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface ICustomerManager
    {
        void AddCustomer(Customer customer);
        void DeleteCustomer(int customerId);
        void UpdateCustomer(Customer customer);
    }
}
