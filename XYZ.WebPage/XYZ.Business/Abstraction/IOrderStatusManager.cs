﻿using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IOrderStatusManager
    {
        void CreateOrderStatus(Order order, int status);
        OrderStatus GetOrderStatus(Order order);
    }
}
