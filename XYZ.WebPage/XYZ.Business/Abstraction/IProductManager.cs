﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IProductManager
    {
        List<Product> GetProducts();
        Product GetProduct(int productId);
    }
}
