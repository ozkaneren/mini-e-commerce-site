﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IBasketManager
    {
        void CreateBasket(Basket basket);
        void UpdateBasket(int basketId, int status);
        Basket GetBasket(int customerId);
        List<Product> GetBasketItems(int customerId);
        bool BasketController(int customerId);
        void AddBasketItem(int productId, int customerId);
        void DeleteBasketItem(int productId, int customerId);
        List<Product> GetBasketItemsWithProductList(int basketId);
        void IncreaseBasketItem(int customerId, int productId);
        void DecreaseBasketItem(int customerId, int productId);
        List<BasketItem> GetBasketItemsWithBasketItemList(int basketId);



    }
}
