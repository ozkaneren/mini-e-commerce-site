﻿using XYZ.Entities;

namespace XYZ.Business.Abstraction
{
    public interface IOrderPaymentManager
    {
        bool CreateOrderPayment(OrderPayment orderPayment,int customerId);
        PaymentStatus GetOrderPayment(int orderPaymentId);
        void UpdateOrderPayment(int OrderPaymentId, int status);

    }
}
