﻿using System;
using XYZ.Business.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;

namespace XYZ.Business.Implamentation
{
    public class OrderManager : IOrderManager
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IBasketManager _basketManager;

        public OrderManager(IOrderRepository orderRepositoryInjection, IBasketManager basketManager)
        {
            _basketManager = basketManager;
            _orderRepository = orderRepositoryInjection;
        }

        public Order GetOrder(int orderId)
        {
            Order orders = new Order();

            orders = _orderRepository.GetOrder(orderId);

            return orders;
        }

        public void UpdateOrder(int orderId, int status)
        {
            _orderRepository.UpdateOrder(orderId,status);

        }

        public void CreateOrder(int customerId, Order order)
        {
            Basket basket;
            DateTime date = DateTime.Now;

            basket = _basketManager.GetBasket(customerId);
            order.BasketId = basket.BasketId;
            order.Date = date;
            order.Status = 3;

            _orderRepository.CreateOrder(order);
        }

        public Order GetOrderWithBasketId(int basketId)
        {
            Order order = new Order();

            order = _orderRepository.GetOrderWithBasketId(basketId);

            return order;
        }
    }
}
