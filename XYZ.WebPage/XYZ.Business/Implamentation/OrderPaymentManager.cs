﻿using System;
using XYZ.Business.Abstraction;
using XYZ.Business.Helper.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Service.Abstraction;
using XYZ.Service.Entity;
using XYZ.Service.Implementation;

namespace XYZ.Business.Implamentation
{
    public class OrderPaymentManager : IOrderPaymentManager
    {
        private readonly IOrderPaymentRepository _orderPaymentRepository;
        private readonly IBankService _bankService;
        private readonly IMailService _mailService;
        private readonly IOrderPriceCalculator _orderPriceCalculator;
        private readonly IBasketManager _basketManager;
        private readonly IOrderManager _orderManager;


        public OrderPaymentManager(IOrderPaymentRepository orderPaymentRepository,
            IBasketManager basketManager,
            IOrderManager orderManager,
            IBankService bankService,
            IMailService mailService,
            IOrderPaymentManager orderPaymentManager)
        {
            _orderPaymentRepository = orderPaymentRepository;
            _bankService = bankService;
            _mailService = mailService;
            _orderPriceCalculator = orderPaymentManager;
            _basketManager = basketManager;
            _orderManager = orderManager;
        }

        public bool CreateOrderPayment(OrderPayment orderPayment, int customerId)
        {

            DateTime date = DateTime.Now;


            BankRequest bankRequest = new BankRequest()
            {

            };

            Basket basket = _basketManager.GetBasket(customerId);
            Order order = _orderManager.GetOrderWithBasketId(basket.BasketId);
            BankResult bankResult = _bankService.DoPayment(bankRequest);

            orderPayment.Amount = _orderPriceCalculator.CalculateOrderPrice(customerId);

            if (bankResult.IsSuccessful)
            {

                _basketManager.UpdateBasket(basket.BasketId, 2);
                orderPayment.PaymentStatusId = 1;
                _orderManager.UpdateOrder(order.OrderId, 1);

            }
            else
            {
                orderPayment.PaymentStatusId = 2;
                _orderManager.UpdateOrder(order.OrderId, 2);
            }

            orderPayment.ResultCode = Convert.ToInt32(bankResult.ResultCode);
            orderPayment.ResultMessage = bankResult.ResultMessage;
            orderPayment.Date = date;
            orderPayment.Provider = _bankService.GetType().ToString();
            orderPayment.OrderId = order.OrderId;


            DoPaymentMailRequest doPaymentMailRequest = new DoPaymentMailRequest()
            {

            };

            _mailService.SendDoPaymentMail(doPaymentMailRequest);
            _orderPaymentRepository.CreateOrderPayment(orderPayment);

            return bankResult.IsSuccessful;
        }

        public PaymentStatus GetOrderPayment(int orderPaymentId)
        {
            PaymentStatus paymentStatus = new PaymentStatus();

            paymentStatus = _orderPaymentRepository.GetOrderPayment(orderPaymentId);

            return paymentStatus;
        }

        public void UpdateOrderPayment(int orderPaymentId, int status)
        {
            _orderPaymentRepository.UpdateOrderPayment(orderPaymentId, status);
        }
    }
}

