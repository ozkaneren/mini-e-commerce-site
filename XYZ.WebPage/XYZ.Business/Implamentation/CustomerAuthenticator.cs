﻿using XYZ.Business.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;

namespace XYZ.Business.Implamentation
{
    public class CustomerAuthenticator : ICustomerAuthenticator
    {
        private readonly ICustomerAuthanticatorRepository _customerAuthanticatorRepository;
        public CustomerAuthenticator(ICustomerAuthanticatorRepository customerAuthanticatorRepository)
        {
            _customerAuthanticatorRepository = customerAuthanticatorRepository;
        }
        public Customer IsAuthenticated(string eMail, string password)
        {
            Customer customer = new Customer();

            customer = _customerAuthanticatorRepository.IsAuthenticated(eMail, password);

            return customer;
        }
    }
}
