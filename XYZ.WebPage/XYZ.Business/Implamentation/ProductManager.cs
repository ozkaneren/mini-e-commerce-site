﻿using System.Collections.Generic;
using XYZ.Business.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;

namespace XYZ.Business.Implamentation
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;

        public ProductManager(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();

            products = _productRepository.GetProducts();

            return products;
        }

        public Product GetProduct(int productId)
        {
            Product product = new Product();
            return product;
        }
    }
}
