﻿using System;
using System.Collections.Generic;
using System.Linq;
using XYZ.Business.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Business.Implamentation
{
    public class BasketManager : IBasketManager
    {
        private readonly IBasketRepository _basketRepository;
        private readonly IBasketItemRepository _basketItemRepository;

        public BasketManager(IBasketRepository basketRepository, IBasketItemRepository basketItemRepository)
        {
            _basketRepository = basketRepository;
            _basketItemRepository = basketItemRepository;
        }

        public void CreateBasket(Basket basket)
        {

            _basketRepository.CreateBasket(basket);
        }

        public void UpdateBasket(int basketId, int status)
        {
            _basketRepository.UpdateBasket(basketId, status);
        }

        public bool BasketController(int customerId)
        {
            Basket basket = _basketRepository.GetBasket(customerId);
            if (basket == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Basket GetBasket(int customerId)
        {

            Basket basket = _basketRepository.GetBasket(customerId);

            if (basket == null)
            {
                basket = new Basket();
                basket.CustomerId = customerId;
                basket.Date = DateTime.Now;
                basket.Status = 1;
                _basketRepository.CreateBasket(basket);
                basket = _basketRepository.GetBasket(customerId);
                return basket;
            }

            List<BasketItem> basketItems = _basketItemRepository.GetBasketItemsWithBasketItemList(basket.BasketId);
            List<Product> products = GetBasketItems(customerId);
            
            IEnumerable<BasketItemProduct> query = from bi in basketItems
                                                   join p in products on bi.ProductId equals p.ProductId
                                                   select new BasketItemProduct() { BasketItem = bi, Product = p };

            basket.BasketItemProducts = query.ToList();

            return basket;
        }


        public List<Product> GetBasketItems(int customerId)
        {
            List<Product> products = new List<Product>();
            Basket basket = new Basket();

            basket = _basketRepository.GetBasket(customerId);

            products = _basketItemRepository.GetBasketItemsWithProductList(basket.BasketId);

            return products;
        }
        public void AddBasketItem(int productId, int customerId)
        {

            Basket basket = GetBasket(customerId);
            _basketItemRepository.AddBasketItem(productId, basket.BasketId);
        }

        public void DeleteBasketItem(int productId, int customerId)
        {
            Basket basket = GetBasket(customerId);

            List<BasketItem> basketItemList = _basketItemRepository.GetBasketItemsWithBasketItemList(basket.BasketId);


            BasketItem first = null;
            foreach (BasketItem p in basketItemList)
            {
                if (p.ProductId.Equals(productId))
                {
                    first = p;
                    break;
                }
            }
            BasketItem basketItem = first;
            _basketItemRepository.DeleteBasketItem(basketItem.BasketItemId, basket.BasketId);
        }

        public List<Product> GetBasketItemsWithProductList(int basketId)
        {
            List<Product> products = _basketItemRepository.GetBasketItemsWithProductList(basketId);

            return products;
        }

        public void IncreaseBasketItem(int customerId, int productId)
        {

            Basket basket = new Basket();
            basket = GetBasket(customerId);
            List<BasketItem> basketItems = GetBasketItemsWithBasketItemList(basket.BasketId);
            BasketItem first = null;
            foreach (BasketItem p in basketItems)
            {
                if (p.ProductId.Equals(productId))
                {
                    first = p;
                    break;
                }
            }
            BasketItem basketItem = first;

            _basketItemRepository.IncreaseBasketItem(productId, basketItem.BasketItemId);
        }

        public void DecreaseBasketItem(int customerId, int productId)
        {
            Basket basket = new Basket();
            basket = GetBasket(customerId);
            List<BasketItem> basketItems = GetBasketItemsWithBasketItemList(basket.BasketId);
            BasketItem first = null;
            foreach (BasketItem p in basketItems)
            {
                if (p.ProductId.Equals(productId))
                {
                    first = p;
                    break;
                }
            }
            BasketItem basketItem = first;
            if (basketItem.Qty > 1)
            {
                _basketItemRepository.DecreaseBasketItem(productId, basketItem.BasketItemId);
            }

        }

        public List<BasketItem> GetBasketItemsWithBasketItemList(int basketId)
        {
            List<BasketItem> basketItemList = _basketItemRepository.GetBasketItemsWithBasketItemList(basketId);
            return basketItemList;
        }

    }
}
