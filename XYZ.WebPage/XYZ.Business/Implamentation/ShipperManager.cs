﻿using System.Collections.Generic;
using XYZ.Business.Abstraction;
using XYZ.Entities;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;

namespace XYZ.Business.Implamentation
{
    public class ShipperManager : IShipperManager
    {
        private readonly IShipperRepository _shipperRepository;

        public ShipperManager(IShipperRepository shipperRepository)
        {
            _shipperRepository = shipperRepository;
        }

        public List<Shipper> GetShippers()
        {
            List<Shipper> shippers;

            shippers = _shipperRepository.GetShippers();

            return shippers;
        }

    }
}
