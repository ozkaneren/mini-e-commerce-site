﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using XYZ.Business.Abstraction;
using XYZ.Business.Helper.Implamentation;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Client.Helper.Implamentation;
using XYZ.Repository.Abstraction;
using XYZ.Repository.Implamentation;
using XYZ.Service.Abstraction;
using XYZ.Service.Implementation;

namespace XYZ.Client
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterDependencyInjectionModule();

            
        }

        private void RegisterDependencyInjectionModule()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<BasketManager>().As<IBasketManager>().SingleInstance();
            builder.RegisterType<BasketRepository>().As<IBasketRepository>().SingleInstance();

            builder.RegisterType<BasketItemRepository>().As<IBasketItemRepository>().SingleInstance();

            builder.RegisterType<ShipperManager>().As<IShipperManager>().SingleInstance();
            builder.RegisterType<ShipperRepository>().As<IShipperRepository>().SingleInstance();

            builder.RegisterType<OrderManager>().As<IOrderManager>().SingleInstance(); 
            builder.RegisterType<OrderRepository>().As<IOrderRepository>().SingleInstance();

            builder.RegisterType<OrderPaymentManager>().As<IOrderPaymentManager>().SingleInstance();
            builder.RegisterType<OrderPaymentRepository>().As<IOrderPaymentRepository>().SingleInstance();

            builder.RegisterType<ProductManager>().As<IProductManager>().SingleInstance();
            builder.RegisterType<ProductRepository>().As<IProductRepository>().SingleInstance();

            builder.RegisterType<CustomerManager>().As<ICustomerManager>().SingleInstance();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>().SingleInstance();

            builder.RegisterType<SessionHelper>().As<ISessionHelper>().SingleInstance();

            builder.RegisterType<CustomerAuthenticator>().As<ICustomerAuthenticator>().SingleInstance();
            builder.RegisterType<CustomerAuthanticatorRepository>().As<ICustomerAuthanticatorRepository>().SingleInstance();

            builder.RegisterType<OrderPriceCalculator>().As<IOrderPriceCalculator>().SingleInstance();

            builder.RegisterType<GarantiBankService>().As<IBankService>().SingleInstance();

           
            DependencyInjectionModule.Initialize(builder);
        }
    }
}
