﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Implamentation;
using XYZ.Client.Models;
using XYZ.Entities;

namespace XYZ.Client.Controllers
{
    [LogInFilter]
    public class ProductController : Controller
    {
        private readonly IProductManager _productManager;
        public const int PageSize = 2;
        public ProductController()
        {
            _productManager = DependencyInjectionModule.Resolve<IProductManager>();
        }

        public ActionResult Index(int currentPageIndex = 1)
        {
            return View(this.GetProducts(currentPageIndex));
        }

        public PagingModel GetProducts(int currentPage = 1)
        {
            int maxRows = 40;


            List<Product> products = _productManager.GetProducts();
            PagingModel model = new PagingModel();

            model.Product = (from product in products
                             select product)
                .OrderBy(r => r.ProductId)
                .Skip((currentPage - 1) * maxRows)
                .Take(maxRows).ToList();

            double pageCount = (double)((decimal)products.Count() / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);

            model.CurrentPageIndex = currentPage;

            return model;
        }

        public PartialViewResult Search(string q)
        {

            List<Product> products = _productManager.GetProducts();

            if (q.IsNullOrWhiteSpace())
            {
                var search = products.Where(r => r.Name.Contains(q) ||
                                                 String.IsNullOrEmpty(q)).Take(100);
                return PartialView("ProductSearchResults", search);
            }
            else
            {
                if (q.Substring(0, 1).Equals(q.Substring(0, 1).ToUpper()))
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToLower();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(100);

                    return PartialView("ProductSearchResults", search);
                }
                else
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToUpper();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(100);

                    return PartialView("ProductSearchResults", search);
                }

            }

        }

    }
}