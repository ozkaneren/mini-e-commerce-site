﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Client.Helper.Implamentation;
using XYZ.Entities;


namespace XYZ.Client.Controllers
{
    [LogInFilter]
    public class OrderController : Controller
    {
        private readonly IOrderManager _orderManager;
        private readonly IShipperManager _shipperManager;
        private readonly ISessionHelper _sessionHelper;

        public OrderController()
        {
            _sessionHelper = DependencyInjectionModule.Resolve<ISessionHelper>();
            _orderManager = DependencyInjectionModule.Resolve<IOrderManager>();
            _shipperManager = DependencyInjectionModule.Resolve<IShipperManager>();
        }
        public ActionResult OrderBasket()
        {
            ViewBag.data = _shipperManager.GetShippers();

            return View();
        }

        public ActionResult OrderBasketPost(Order order)
        {
            Customer customer = _sessionHelper.Get<Customer>();
            _orderManager.CreateOrder(customer.CustomerId, order);
            return RedirectToAction("PayOrder", "OrderPayment");
        }
    }
}