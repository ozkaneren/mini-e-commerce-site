﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Client.Helper.Implamentation;
using XYZ.Entities;

namespace XYZ.Client.Controllers
{
    [LogInFilter]
    public class OrderPaymentController : Controller
    {
        private readonly IOrderPaymentManager _orderPaymentManager;
        private readonly ISessionHelper _sessionHelper;


        public OrderPaymentController()
        {
            _orderPaymentManager = DependencyInjectionModule.Resolve<IOrderPaymentManager>();
            _sessionHelper = DependencyInjectionModule.Resolve<ISessionHelper>();
        }
        public ActionResult PayOrder()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PayOrder(OrderPayment orderPayment)
        {
            Customer customer = _sessionHelper.Get<Customer>();
            bool pageDecider;
            pageDecider = _orderPaymentManager.CreateOrderPayment(orderPayment, customer.CustomerId);

            if (pageDecider)
            {
               
                return RedirectToAction("Success");
            }
            else
            {
                NotificationHelper.PushErrorMessage("Error");
                return View();
            }

        }

        public ActionResult Success()
        {
            NotificationHelper.PushSuccessMessage("Success");
            return View();
        }
    }
}