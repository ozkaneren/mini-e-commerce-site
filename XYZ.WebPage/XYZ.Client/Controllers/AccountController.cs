﻿using System;
using System.Web.Mvc;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Client.Helper.Implamentation;
using XYZ.Entities;
using XYZ.Repository.Implamentation;

namespace XYZ.Client.Controllers
{
    public class AccountController : Controller
    {
        private readonly ICustomerAuthenticator _customerAuthenticator;

        private readonly ISessionHelper _sessionHelper;

        private readonly ICustomerManager _customerManager;

        public AccountController()
        {
            //ICustomerAuthenticator customerAuthenticator, ISessionHelper sessionHelper, ICustomerManager customerManager



            //_customerAuthenticator = new CustomerAuthenticator(new CustomerAuthanticatorRepository());
            //_sessionHelper = new SessionHelper();
            //_customerManager = new CustomerManager(new CustomerRepository());

            _customerAuthenticator = DependencyInjectionModule.Resolve<ICustomerAuthenticator>();
            _sessionHelper = DependencyInjectionModule.Resolve<ISessionHelper>();
            _customerManager = DependencyInjectionModule.Resolve<ICustomerManager>();
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(Customer customerLogin)
        {
            try
            {
                Customer customer = _customerAuthenticator.IsAuthenticated(customerLogin.Email, customerLogin.Password);

                _sessionHelper.Add<Customer>(customer);
                //...
                //...

                return RedirectToAction("Index", "Product", new { });

            }
            catch (Exception ex)
            {

                return View();
            }
        }

        public ActionResult LogOut()
        {
            _sessionHelper.Remove<Customer>();

            return RedirectToAction("LogIn");
        }

        [HttpPost]
        public ActionResult SignIn(Customer customer)
        {
            try
            {
                _customerManager.AddCustomer(customer);
                return RedirectToAction("LogIn");
            }

            catch (Exception e)
            {
                return View();
            }

        }

        public ActionResult SignIn()
        {

            return View();
        }


        [HttpPost]
        public ActionResult DeleteAccount(int customerId)
        {
            _customerManager.DeleteCustomer(customerId);
            return View();
        }

        public ActionResult DeleteAccount()
        {

            return View();
        }
    }
}