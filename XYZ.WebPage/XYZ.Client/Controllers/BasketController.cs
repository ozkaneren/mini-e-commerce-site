﻿using System.Collections.Generic;
using System.Web.Mvc;
using XYZ.Business.Abstraction;
using XYZ.Business.Implamentation;
using XYZ.Client.Helper.Abstraction;
using XYZ.Client.Helper.Implamentation;
using XYZ.Entities;
using XYZ.Repository.Implamentation;

namespace XYZ.Client.Controllers
{
    [LogInFilter]
    public class BasketController : Controller
    {
        private readonly IBasketManager _basketManager;
  
        private readonly ISessionHelper _sessionHelper;

        public BasketController()
        {
            _basketManager = DependencyInjectionModule.Resolve<IBasketManager>();
            _sessionHelper = DependencyInjectionModule.Resolve<ISessionHelper>();
        }

        [HttpGet]
        public ActionResult MyBasket()
        {

            Basket basket;
            Customer customer = _sessionHelper.Get<Customer>();

            if (_basketManager.BasketController(customer.CustomerId))
            {
               basket = _basketManager.GetBasket(customer.CustomerId);
                return View(basket);
            }
            else
            {
                return View("ShowNoBasket");
            }

        }

        public ActionResult ShowNoBasket()
        {

            return View();
        }

        [HttpGet]
        public ActionResult AddBasketItem(int productId)
        {

            Customer customer = _sessionHelper.Get<Customer>();
            _basketManager.AddBasketItem(productId, customer.CustomerId);

            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        public ActionResult DeleteBasketItem(int productId)
        {

            Customer customer = _sessionHelper.Get<Customer>();
            _basketManager.DeleteBasketItem(productId, customer.CustomerId);

            return RedirectToAction("MyBasket", "Basket", new { });
        }

        [HttpGet]
        public ActionResult IncreaseBasketItem(int productId)
        {
            Customer customer = _sessionHelper.Get<Customer>();

            _basketManager.IncreaseBasketItem(customer.CustomerId, productId);

            return RedirectToAction("MyBasket", "Basket");

        }

        [HttpGet]
        public ActionResult DecreaseBasketItem(int productId)
        {

            Customer customer = _sessionHelper.Get<Customer>();
            _basketManager.DecreaseBasketItem(customer.CustomerId, productId);

            return RedirectToAction("MyBasket", "Basket");
        }
    }
}