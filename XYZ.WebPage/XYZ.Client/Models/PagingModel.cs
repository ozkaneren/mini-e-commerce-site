﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Client.Models
{
    public class PagingModel
    {
        public List<Product> Product { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
    }
}