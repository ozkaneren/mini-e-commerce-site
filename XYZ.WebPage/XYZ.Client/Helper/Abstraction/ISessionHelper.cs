﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XYZ.Client.Helper.Abstraction
{
    public interface ISessionHelper
    {
        void Add<T>(T item);
        T Get<T>();
        void Remove<T>();
    }
}