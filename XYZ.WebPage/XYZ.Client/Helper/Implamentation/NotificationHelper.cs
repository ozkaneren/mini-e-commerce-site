﻿namespace XYZ.Client.Helper.Implamentation
{
    public class NotificationHelper
    {
        public static string SuccessMessage { get; private set; }

        public static string ErrorMessage { get; private set; }

        public static void PushSuccessMessage(string message)
        {
            SuccessMessage = message;
        }

        public static void PushErrorMessage(string message)
        {
            ErrorMessage = message;
        }

        public static string PopSuccessMessage()
        {
            string message = SuccessMessage;

            SuccessMessage = null;

            return message;
        }

        public static string PopErrorMessage()
        {
            string message = ErrorMessage;

            ErrorMessage = null;

            return message;
        }

        public static bool IsThereAnySuccessMessage()
        {
            return !string.IsNullOrEmpty(SuccessMessage);
        }

        public static bool IsThereAnyErrorMessage()
        {
            return !string.IsNullOrEmpty(ErrorMessage);
        }
    }
}