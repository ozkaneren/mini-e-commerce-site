﻿using Autofac;

namespace XYZ.Client.Helper.Implamentation
{
    public static class DependencyInjectionModule
    {
        private static IContainer Container { get; set; }

        public static void Initialize(ContainerBuilder containerBuilder)
        {
            Container = containerBuilder.Build();
        }

        public static T Resolve<T>() where T : class
        {
            return Container.Resolve<T>();
        }
    }
}