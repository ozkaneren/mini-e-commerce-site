﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using XYZ.Client.Helper.Abstraction;

namespace XYZ.Client.Helper.Implamentation
{
    public class SessionHelper : ISessionHelper
    {
        private static string sessionKey { get; set; }
        private string value { get; set; }


        public void Add<T>(T item)
        {
            
            sessionKey = typeof(T).Name;

            HttpContext.Current.Session[sessionKey] = item;


        }

        public T Get<T>()
        {
            

            return (T)HttpContext.Current.Session[sessionKey];
        }


        public void Remove<T>()
        {
            HttpContext.Current.Session[sessionKey] = null;

            value = null;

            sessionKey = null;
        }
    }
}