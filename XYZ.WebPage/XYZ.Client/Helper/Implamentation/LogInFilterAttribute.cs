﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XYZ.Client.Helper.Abstraction;
using XYZ.Entities;

namespace XYZ.Client.Helper.Implamentation
{
    public class LogInFilterAttribute : ActionFilterAttribute
    {
        private readonly ISessionHelper _sessionHelper;
        public LogInFilterAttribute()
        {
            _sessionHelper = new SessionHelper();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Customer customer = _sessionHelper.Get<Customer>();
            if (customer == null)
            {
                filterContext.Result = new RedirectResult("/Account/LogIn", false);
            }
        }
    }
}