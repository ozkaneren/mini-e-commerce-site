﻿using System;
using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IOrderRepository
    {
        Order GetOrder(int orderId);
        void UpdateOrder(int orderId, int status);
        void CreateOrder(Order order);
        Order GetOrderWithBasketId(int basketId);

    }
}
