﻿using System;
using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IBasketRepository
    {
        void CreateBasket(Basket basket);
        void UpdateBasket(int basketId, int status);
        Basket GetBasket(int customerId);
    }
}
