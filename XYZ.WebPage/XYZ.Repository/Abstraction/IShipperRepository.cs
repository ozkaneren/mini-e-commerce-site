﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IShipperRepository
    {
        List<Shipper> GetShippers();
    }
}
