﻿using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface ICustomerAuthanticatorRepository
    {
        Customer IsAuthenticated(string eMail, string password);
    }
}
