﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IProductRepository
    {
        List<Product> GetProducts();
        Product GetProduct(int productId);

    }
}