﻿using System.Collections.Generic;
using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IBasketItemRepository
    {
        void AddBasketItem(int productId, int basketId);
        void DeleteBasketItem(int basketItemId, int basketId);
        List<Product> GetBasketItemsWithProductList(int basketId);
        List<BasketItem> GetBasketItemsWithBasketItemList(int basketId);
        void IncreaseBasketItem(int productId, int basketItemId);
        void DecreaseBasketItem(int productId, int basketItemId);
    }
}
