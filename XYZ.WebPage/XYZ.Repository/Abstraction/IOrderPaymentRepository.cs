﻿using XYZ.Entities;

namespace XYZ.Repository.Abstraction
{
    public interface IOrderPaymentRepository
    {
        void CreateOrderPayment(OrderPayment orderPayment);
        PaymentStatus GetOrderPayment(int orderPaymentId);
        void UpdateOrderPayment(int orderPaymentId, int status);
    }
}
