﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class OrderPaymentRepository : IOrderPaymentRepository

    {
        private readonly string connectionString =
            "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public void CreateOrderPayment(OrderPayment orderPayment)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(
                    @"INSERT INTO OrderPayment (OrderId,Amount,[Provider],[Date],BinNumber,LastFour,ExpYear,CvcNumber,ResultCode,ResultMessage,PaymentStatusId,ExpMonth) 
VALUES (@OrderId,@Amount,@Provider,@Date,@BinNumber,@LastFour,@ExpYear,@CvcNumber,@ResultCode,@ResultMessage,@PaymentStatusId,@ExpMonth)",
                    new
                    {
                        orderPayment.OrderId,
                        orderPayment.Amount,
                        orderPayment.Provider,
                        orderPayment.Date,
                        orderPayment.BinNumber,
                        orderPayment.LastFour,
                        orderPayment.ExpYear,
                        orderPayment.CvcNumber,
                        orderPayment.PaymentStatusId,
                        orderPayment.ExpMonth,
                        orderPayment.ResultCode,
                        orderPayment.ResultMessage
                    });
            }
        }

        public PaymentStatus GetOrderPayment(int orderPaymentId)
        {

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<PaymentStatus> paymentStatuses =
                    con.Query<PaymentStatus>(@"SELECT * FROM OrderPayment WHERE OrderPaymentId = @OrderPaymentId", new
                    {
                        OrderPaymentId = orderPaymentId
                    });
                return paymentStatuses.FirstOrDefault();

            }

        }

        public void UpdateOrderPayment(int orderPaymentId, int status)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query<PaymentStatus>(
                    @"UPDATE [OrderPayment] SET PaymentStatusId = @PaymentStatusId WHERE OrderPaymentId = @OrderPaymentId",
                    new
                    {
                        PaymentStatusId = status,
                        OrderPaymentId = orderPaymentId
                    });

            }
        }
    }
}