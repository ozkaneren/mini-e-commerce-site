﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string connectionString =
            "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";


        public Order GetOrder(int orderId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Order> orders = con.Query<Order>("SELECT * FROM [Order] WHERE OrderId = @OrderId", new
                {
                    OrderId = orderId
                });
                return orders.FirstOrDefault();
            }
        }

        public void UpdateOrder(int orderId, int status)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("UPDATE [Order] SET Status = @Status WHERE OrderId = @OrderId ", new
                {
                    Status = status,
                    OrderId = orderId
                });
            }
        }

        public void CreateOrder(Order order)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("INSERT INTO [Order] (BasketId,Status,Date,ShipperId) VALUES(@BasketId,@StatusId,@Date,@ShipperId)", new
                {
                    BasketId = order.BasketId,
                    StatusId = order.Status,
                    Date = order.Date,
                    ShipperId = order.ShipperId

                });
            }
        }

        public Order GetOrderWithBasketId(int basketId)
        {

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Order> orders = con.Query<Order>("SELECT * FROM [Order] WHERE BasketId = @BasketId", new
                {
                    BasketId = basketId
                });
                return orders.FirstOrDefault();
            }
        }
    }
}
