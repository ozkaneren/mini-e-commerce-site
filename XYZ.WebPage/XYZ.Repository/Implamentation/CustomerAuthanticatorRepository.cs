﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class CustomerAuthanticatorRepository : ICustomerAuthanticatorRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";


        public Customer IsAuthenticated(string eMail, string password)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {

                    IEnumerable<Customer> customer = con.Query<Customer>("SELECT * FROM Customer WHERE Email = @Email AND Password= @Password", new
                    {
                        Email = eMail,
                        Password = password
                    });


                    return customer.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }
    }
}
