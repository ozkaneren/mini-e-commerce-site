﻿using System.Data.SqlClient;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class CustomerRepository : ICustomerRepository

    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public void AddCustomer(Customer customer)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("INSERT INTO Customer (Email, Password) VALUES(@Email, @Password)", new
                {
                    Email = customer.Email,
                    Password = customer.Password
                });
            }
        }

        public void DeleteCustomer(int customerId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("DELETE FROM Customer WHERE CustomerId = @CustomerId)", new
                {
                    CustomerId = customerId
                });
            }
        }

        public void UpdateCustomer(Customer customer)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("UPDATE Customer SET Email = @Email, Password = @Password WHERE CustomerId = @CustomerId", new
                {
                    Email = customer.Email,
                    Password = customer.Password
                });
            }
        }

    }
}
