﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class ProductRepository : IProductRepository
    {
        private readonly string connectionString =
            "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public List<Product> GetProducts()
        {

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> product = con.Query<Product>(@"SELECT * FROM Product");

                return product.ToList();
            }
        }

        public Product GetProduct(int productId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> product = con.Query<Product>(@"SELECT * FROM Product WHERE ProductId = @ProductId", new
                {
                    ProductId = productId
                });

                return product.FirstOrDefault();
            }
        }
    }
}