﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class ShipperRepository : IShipperRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";
        
        public List<Shipper> GetShippers()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                IEnumerable<Shipper> shippers = con.Query<Shipper>("SELECT * FROM Shipper");

                return shippers.ToList();
            }
        }

    }
}
