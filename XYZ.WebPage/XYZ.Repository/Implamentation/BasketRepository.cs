﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class BasketRepository : IBasketRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;Initial Catalog = XYZDB;Integrated Security = SSPI;";

        public void CreateBasket(Basket basket)
        {

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("INSERT INTO Basket(CustomerId,Date,Status) VALUES(@CustomerId, @Date, @Status)", new
                {
                    CustomerId = basket.CustomerId,
                    Date = basket.Date,
                    Status = basket.Status
                });
            }
        }

        public void UpdateBasket(int basketId, int status)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("UPDATE Basket SET Status = @Status WHERE BasketId = @BasketId", new
                {
                    BasketId = basketId,
                    Status = status
                });
            }
        }

        public Basket GetBasket(int customerId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Basket> baskets = con.Query<Basket>("SELECT * FROM Basket WHERE CustomerId = @CustomerId AND Status = @Status", new
                {
                    Status = 1,
                    CustomerId = customerId
                });
                return baskets.LastOrDefault();
            }
        }
    }
}
