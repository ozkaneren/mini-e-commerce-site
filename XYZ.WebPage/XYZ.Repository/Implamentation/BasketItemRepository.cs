﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using XYZ.Entities;
using XYZ.Repository.Abstraction;

namespace XYZ.Repository.Implamentation
{
    public class BasketItemRepository : IBasketItemRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog = XYZDB;" + "Integrated Security = SSPI;";

        public void AddBasketItem(int productId, int basketId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("INSERT INTO BasketItem (BasketId,ProductId,Qty) VALUES(@BasketId, @ProductId, @Qty)", new
                {
                    BasketId = basketId,
                    ProductId = productId,
                    Qty = 1
                });
            }
        }

        public void DeleteBasketItem(int basketItemId, int basketId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("DELETE FROM BasketItem WHERE BasketItemId = @BasketItemId", new
                {
                    BasketItemId = basketItemId

                });

            }
        }

        public List<Product> GetBasketItemsWithProductList(int basketId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<Product> products = con.Query<Product>(@"
                    SELECT p.Barcode,p.CategoryId, p.ColorId, p.Name, p.Price, p.ProductId, p.Size, p.Stock, p.Stock, p.SupplierId FROM BasketItem as bi
                    INNER JOIN Basket as b ON bi.BasketId = b.BasketId
                    INNER JOIN Customer as c ON b.CustomerId = c.CustomerId
                    INNER JOIN Product p ON bi.ProductId = p.ProductId
                    WHERE b.BasketId = @BasketId AND b.Status = 1
                    ", new
                {
                    BasketId = basketId
                });

                return products.ToList();
            }
        }

        public List<BasketItem> GetBasketItemsWithBasketItemList(int basketId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                IEnumerable<BasketItem> basketItems = con.Query<BasketItem>(@"SELECT * FROM BasketItem WHERE @BasketId = BasketId
                    ", new
                {
                    BasketId = basketId
                });

                return basketItems.ToList();
            }
        }

        public void IncreaseBasketItem(int productId, int basketItemId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
               con.Query(@"UPDATE BasketItem SET Qty = Qty + 1 WHERE ProductId = @ProductId AND BasketItemId = @BasketItemId
                    ", new
                {
                    ProductId = productId,
                    BasketItemId = basketItemId
                });

            }
        }

        public void DecreaseBasketItem(int productId, int basketItemId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query(@"UPDATE BasketItem SET Qty = Qty - 1 WHERE ProductId = @ProductId AND BasketItemId = @BasketItemId
                    ", new
                {

                    ProductId = productId,
                    BasketItemId = basketItemId
                });

            }
        }
    }
}
